#!/bin/bash

# Copyright (C) 2019 Karel Zak <kzak@redhat.com>

srcdir="../source"
testdir="${srcdir}/tests"

echo "srcdir:  $srcdir"
echo "testdir: $testdir"

if [ ! -x "${testdir}/run.sh" ]; then
	echo "upstream tests not found"
	exit 1
fi

# We don't compile the tests, but it still needs some hints.
#
echo > ${srcdir}/config.h
echo "#define HAVE_WIDECHAR 1"              >> ${srcdir}/config.h
echo "#define HAVE_LINUX_NET_NAMESPACE_H 1" >> ${srcdir}/config.h

# Remove stuff we don't have in RHEL
#
rm -rf ${testdir}/ts/misc/line


version_tests=$(cat ${srcdir}/.version)
version_system=$(rpm -q util-linux)

echo
echo "Used versions: "
echo "       upstream tests: $version_tests"
echo " installed util-linux: $version_system"
echo

# Run upstream tests
#
${testdir}/run.sh --use-system-commands --noskip-commands --show-diff

exit $?
